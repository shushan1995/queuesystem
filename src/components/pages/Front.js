import React from 'react';
import { Paper } from 'material-ui';
import Table from './Table'
import ReactPlayer from 'react-player';

class Front extends React.Component {
    state = {}
    render() {
        return (

            <div className={'inline-it'}>
                <div style={{ width: '40%' }} className='column-it'>
                    <Paper zDepth={1} style={{ width: '100%', height: '100%' }}>
                        <Table />
                    </Paper>
                </div>
                <div style={{ width: '60%' }} className={'column-it'}>
                    <div className='inline-it'>
                        <Paper zDepth={1} style={{ width: '35%' }}>
                            <img className='photo' src="/sunrise.jpg" />
                        </Paper><br />
                        <Paper zDepth={1} style={{ width: '65%' }}>
                            <img className='photo2' src="/sunrise2.jpg" />
                        </Paper>
                    </div>
                    <div>
                        <Paper zDepth={1} style={{ width: '100%', height: '600px' }}>
                            <ReactPlayer loop playing muted width='100%' height='100%' url="https://www.youtube.com/watch?v=GvNmwyPaNeQ" />
                        </Paper>
                    </div>
                </div>
            </div>


        );
    }
}

export default Front;