import React from 'react';
import { Paper } from 'material-ui';
import { toJS } from 'mobx';
import { observer, inject } from "mobx-react";
import io from "socket.io-client";
import { URL } from './../../lib/config'

class Table extends React.Component {
    componentDidMount() {
        const socket = io(URL);
        // console.log('>>>>', URL);
        socket.on('', msg => {
            // console.log('msg>>', msg);
            let data = JSON.parse(msg)
            if (data[1].token_no != '----') {

                const temp = ({
                    token_no: data[1].token_no,
                    counter: data[1].counter,
                    blink_count: data["blink_config"].blink_count,
                    blink_off_ms: data["blink_config"].blink_off_ms,
                    blink_on_ms: data["blink_config"].blink_on_ms,
                })

                this.props.store.pushToStore(temp);
            }
        })

        let i = 1000;
        let c = 1;

        setInterval(() => {
            const temp = {
                token_no: `A${i}`,
                counter: Number(`${c}`),
                blink_count: 5,
                blink_on_ms: 1000,
                blink_off_ms: 500
            };

            this.props.store.pushToStore(temp);
            if (c < 4) {
                c += 1
            }
            else {
                c = 1
            }
            i += 1;
        }, 5000)

    }

    render() {
        // console.log(toJS(this.props.store.stores));
        let a = toJS(this.props.store.stores);
        console.log(a);
        let b = null, c = null, d = null, e = null;
        if (a.length > 0) {
            a.forEach(obj => {
                if (obj.counter === 1) {
                    b = obj.token_no;
                }
                if (obj.counter === 2) {
                    c = obj.token_no;
                }
                if (obj.counter === 3) {
                    d = obj.token_no;
                }
                if (obj.counter === 4) {
                    e = obj.token_no;
                }

            })
        }
        console.log(b, c, d, e)

        return (

            <table>
                {/* <tbody> */}
                <tr id='red'>
                    <td colSpan="2" >
                        <h1 className="myTableHead">TOKEN</h1>
                    </td>
                </tr>
                <tr id='red'>
                    <td colSpan="2">
                        <h1 className="myTableHead">{a.length > 0 ? (a.map(x => x.token_no).slice(-1)[0]) : '---'}</h1>
                    </td>
                </tr>
                <tr id='red'>
                    <td colSpan="2">
                        <h1 className="myTableHead">{a.length > 0 ? 'Counter' + ' ' + (a.map(x => x.counter).slice(-1)[0]) : '---'}</h1>
                    </td>
                </tr>
                <tr id='blue'>
                    <td>
                        <h2 className="myTableHead">Token No.</h2>
                    </td>
                    <td>
                        <h2 className="myTableHead">Counter</h2>
                    </td>
                </tr>
                <tr id='blue' >
                    <td>
                        <h2 className="myTableHead">{(!!b ? b : '---')}</h2>
                    </td>
                    <td>
                        <h2 className="myTableHead">Counter 1</h2>
                    </td>
                </tr>
                <tr id='blue'>
                    <td>
                        <h2 className="myTableHead">{(!!c ? c : '---')}</h2>
                    </td>
                    <td>
                        <h2 className="myTableHead">Counter 2</h2>
                    </td>
                </tr>
                <tr id='blue'>
                    <td>
                        <h2 className="myTableHead">{(!!d ? d : '---')}</h2>
                    </td>
                    <td>
                        <h2 className="myTableHead">Counter 3</h2>
                    </td>
                </tr>
                <tr id='blue'>
                    <td>
                        {/* <h2 className="myTableHead">{a.length > 0 ? (a.map(x => {
                            if (x.counter === 4) {
                                return x.token_no;
                            }
                        }).slice(-1)[0]) : '---'}</h2> */}
                        <h2 className="myTableHead">{(!!e ? e : '---')}</h2>
                    </td>
                    <td>
                        <h2 className="myTableHead">Counter 4</h2>
                    </td>
                </tr>
                {/* <h5 className="myTableHead">
                    <div className="mini-heading">
                        <div >
                            Token No.
                        </div>
                        <div className="inline-it">Counter</div>
                    </div>
                </h5> */}
                {/* </tbody> */}
            </table>


        );
    }

}

export default inject('store')(observer(Table));