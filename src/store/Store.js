import { observable, action, makeObservable } from 'mobx';

class Store {
    stores = [];

    pushToStore = (item) => {
        this.stores = [...this.stores, item];
    }

    constructor() {
        makeObservable(this, {
            stores: observable,
            pushToStore: action
        })
    }
}

export default Store;

// export default class Store {
//     @observable stores = [];

//     @action tokendata() {
//         socket.on('rts/multidiveter/display', msg => {
//             console.log('msg>>', msg);
//             let data = JSON.parse(msg)
//             if (data[1].token_no != '----') {

//                 const temp = ({
//                     token_no: data[1].token_no,
//                     counter: data[1].counter,
//                     blink_count: data["blink_config"].blink_count,
//                     blink_off_ms: data["blink_config"].blink_off_ms,
//                     blink_on_ms: data["blink_config"].blink_on_ms,
//                 })


//                 // console.log("data>>", this.stores);
//                 this.stores.push(temp);
//                 // console.log("data>>", this.stores);
//                 console.log(JSON.stringify(this.stores, null, 5))
//             }
//             // this.stores = msg;
//             // console.log(toJS(this.stores))
//         })
//     }
// }