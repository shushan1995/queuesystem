import React from 'react';
import Front from './components/pages/Front';
import Footer from './components/_common/Footer';

class App extends React.Component {
  render() {
    return (
      // <div className={'fullContain'}>
      <div>
        <div>
          <Front />
        </div>

        <Footer />
      </div>
    );
  }
}

export default App;
